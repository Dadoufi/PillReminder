/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pillreminder.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager.WakeLock;
import android.util.Log;

import com.pillreminder.AsyncHandler;

public class AlarmInitReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(final Context context, Intent intent) {
        final String action = intent.getAction();

        final PendingResult result = goAsync();
        final WakeLock wl = AlarmAlertWakeLock.createPartialWakeLock(context);
        wl.acquire();

        // We need to increment the global id out of the async task to prevent
        // race conditions
        Log.d("init AlarmInitReceiver", "enterd init receiver");

        AsyncHandler.post(new Runnable() {
            @Override
            public void run() {
                // Remove the snooze alarm after a boot.
                if (action.equals(Intent.ACTION_BOOT_COMPLETED)) {
                    // Clear stopwatch and timers data
                    AsyncHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("init receiver", "enterd init receiver");
                            if (action.equals(Intent.ACTION_BOOT_COMPLETED)) {

                                AlarmPillService.createAllAlarms(context);
                            }
                        }
                    });

                    result.finish();

                    wl.release();
                }
            }
        });

    }


}
