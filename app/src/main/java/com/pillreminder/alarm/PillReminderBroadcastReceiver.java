package com.pillreminder.alarm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.pillreminder.AsyncHandler;
import com.pillreminder.R;
import com.pillreminder.model.Pill;


public class PillReminderBroadcastReceiver extends BroadcastReceiver {

    private final String TAG = PillReminderBroadcastReceiver.this.getClass().getSimpleName();

    @Override
    public void onReceive(final Context context, final Intent intent) {


        Log.d(TAG, "entered AlarmBroadcastReceiver");
        final PendingResult result = goAsync();
        final PowerManager.WakeLock wl = AlarmAlertWakeLock.createPartialWakeLock(context);
        wl.acquire();
        AsyncHandler.post(new Runnable() {
            @Override
            public void run() {
                handleIntent(context, intent);
                result.finish();
                wl.release();
            }
        });


    }


    private void handleIntent(Context context, Intent intent) {
        Pill pill = intent.getParcelableExtra(Pill.PILL_INSTANCE);

        context.sendBroadcast(new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));

        Log.d(TAG, "entered handle intent");

        Intent searchIntent = new Intent(context, AlarmPillService.class);
        searchIntent.setAction(AlarmPillService.GOOGLE_SEARCH_ACTION);
        searchIntent.putExtra(Pill.PILL_INSTANCE, pill.id);
        PendingIntent contentIntent = PendingIntent.getService(context, pill.hashCode(), searchIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        Intent stopAlarmIntent = new Intent(context, AlarmPillService.class);
        stopAlarmIntent.setAction(AlarmPillService.STOP_ALARM_ACTION);
        stopAlarmIntent.putExtra(Pill.PILL_INSTANCE, pill.id);
        PendingIntent stopAlarmPendingIntent = PendingIntent.getService(context, pill.hashCode(), stopAlarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        String title = pill.name;
        String content = pill.name;

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentTitle(context.getResources().getString(R.string.pill_notification_title, title))
                .setContentText(context.getResources().getString(R.string.pill_notification_content, content))
                .setSmallIcon(R.drawable.ic_pill)
                .setWhen(0)
                .setOngoing(true)
                .setAutoCancel(false)
                //.setDeleteIntent(stopAlarmPendingIntent)
                .addAction(android.R.drawable.ic_menu_view, context.getResources().getString(R.string.pill_notification_details), contentIntent)
                .addAction(R.drawable.ic_alarm_off, context.getResources().getString(R.string.pill_notification_stop_reminder), stopAlarmPendingIntent)
                .setDefaults(NotificationCompat.DEFAULT_LIGHTS)
                .setCategory(NotificationCompat.CATEGORY_ALARM)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setVibrate(new long[0]);


        NotificationManager notifyMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notifyMgr.cancel((int) pill.id);

        notifyMgr.notify((int) pill.id, builder.build());

        AlarmPillService.startAlarm(context, pill);


    }


}
