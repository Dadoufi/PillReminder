/*
 * Copyright (c) 2014, The Linux Foundation. All rights reserved.
 * Not a Contribution.
 *
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pillreminder.alarm;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.pillreminder.AlarmHelper;
import com.pillreminder.events.PillEnabledEvent;
import com.pillreminder.model.Pill;
import com.pillreminder.model.Pill$Table;

import java.util.List;

import de.greenrobot.event.EventBus;


/**
 * This service is in charge of starting/stoping the alarm. It will bring up and manage the
 */
public class AlarmPillService extends Service {

    public static final String CREATE_ALARM_ACTION = "CREATE_ALARM_ACTION";
    public static final String DELETE_ALARM_ACTION = "DELETE_ALARM_ACTION";
    public static final String CREATE_ALL_ALARMS_ACTION = "CREATE_ALL_ALARMS_ACTION";
    public static final String START_ALARM_ACTION = "START_ALARM_ACTION";
    public static final String STOP_ALARM_ACTION = "STOP_ALARM_ACTION";
    public static final String GOOGLE_SEARCH_ACTION = "GOOGLE_SEARCH_ACTION";
    public static final String ALL_PILL_IDS = "ALL_PILL_IDS";

    private Pill mCurrentPill = null;

    public static void createAlarm(Context context, Pill pill) {
        Intent intent = AlarmHelper.createIntent(context, AlarmPillService.class, pill.getId());
        intent.setAction(CREATE_ALARM_ACTION);

        // Maintain a cpu wake lock until the service can get it
        AlarmAlertWakeLock.acquireCpuWakeLock(context);
        context.startService(intent);
    }

    public static void deleteAlarm(Context context, Pill pill) {
        Intent intent = AlarmHelper.createIntent(context, AlarmPillService.class, pill.getId());
        intent.setAction(DELETE_ALARM_ACTION);

        // We don't need a wake lock here, since we are trying to kill an alarm
        context.startService(intent);
    }

    public static void stopCurrentAlarm(Context context, Pill pill) {
        Intent intent = AlarmHelper.createIntent(context, AlarmPillService.class, pill.getId());
        intent.setAction(STOP_ALARM_ACTION);

        // We don't need a wake lock here, since we are trying to kill an alarm
        context.startService(intent);

    }

    public static void startAlarm(Context context, Pill pill) {
        Intent intent = AlarmHelper.createIntent(context, AlarmPillService.class, pill.id);
        intent.setAction(START_ALARM_ACTION);
        AlarmAlertWakeLock.acquireCpuWakeLock(context);

        context.startService(intent);
    }

    public static void createAllAlarms(Context context) {
        Intent intent = AlarmHelper.createIntent(context, AlarmPillService.class);
        intent.setAction(CREATE_ALL_ALARMS_ACTION);
        AlarmAlertWakeLock.acquireCpuWakeLock(context);
        Log.d("entered", "create all alarms");
        context.startService(intent);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        long pillId = intent.getLongExtra(Pill.PILL_INSTANCE, -1);



        Pill pill = Pill.getPillById(pillId);
        if (CREATE_ALARM_ACTION.equals(intent.getAction())) {
            if (pill != null) {
                createAlarm(pill);
                AlarmAlertWakeLock.releaseCpuLock();
                return Service.START_NOT_STICKY;

            }

        } else if (DELETE_ALARM_ACTION.equals(intent.getAction())) {
            if (pill != null) {
                if (mCurrentPill != null && pill.id == mCurrentPill.id) {
                    stopCurrentAlarm();
                    return Service.START_NOT_STICKY;
                }

                deleteAlarm(pill);

            }
            stopSelf();
        } else if (START_ALARM_ACTION.equals(intent.getAction())) {
            if (pill == null) {
                if (mCurrentPill != null) {
                    // Only release lock if we are not firing alarm
                    AlarmAlertWakeLock.releaseCpuLock();
                }
                return Service.START_NOT_STICKY;
            } else if (mCurrentPill != null && mCurrentPill.id == pillId) {
                return Service.START_NOT_STICKY;
            }
            startAlarm(pill);
        } else if (STOP_ALARM_ACTION.equals(intent.getAction())) {
            if (pill != null) {
                stopCurrentAlarm();
            }
            return Service.START_NOT_STICKY;

        } else if (GOOGLE_SEARCH_ACTION.equals(intent.getAction())) {
            if (pill != null) {
                startGoogleSearchActivity(pill);
                stopCurrentAlarm();
            }
            return Service.START_NOT_STICKY;
        } else if (CREATE_ALL_ALARMS_ACTION.equals(intent.getAction())) {
            List<Pill> pillList = Pill.getAllPills();
            if (pillList != null && !pillList.isEmpty()) {

                for (Pill pillValue : pillList) {
                    createAlarm(pillValue);
                }
            }
            AlarmAlertWakeLock.releaseCpuLock();
            return START_NOT_STICKY;
        }

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // stopCurrentAlarm();
    }

    private void startGoogleSearchActivity(Pill pill) {
        Uri uri = Uri.parse("http://www.google.com/#q=" + pill.name);
        Intent googleIntent = new Intent(Intent.ACTION_VIEW);
        googleIntent.setData(uri);
        googleIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(googleIntent);
    }

    private void createAlarm(Pill pill) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        //AlarmNotifications.showAlarmNotification(this, mCurrentPill);
        Intent intent = new Intent(this, PillReminderBroadcastReceiver.class);
        intent.putExtra(Pill.PILL_INSTANCE, pill);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, (int) pill.id, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (pill.getEnabled()) {
            if (pill.getRepeat()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, pill.isScheduleTommorow() ? pill.getTomorrowAlarmTime().getTimeInMillis() : pill.getAlarmTime().getTimeInMillis(), pendingIntent);
                } else {
                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, pill.getAlarmTime().getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, pill.getAlarmTime().getTimeInMillis(), pendingIntent);
                } else {
                    alarmManager.set(AlarmManager.RTC_WAKEUP, pill.getAlarmTime().getTimeInMillis(), pendingIntent);
                }
            }

        }

    }

    private void deleteAlarm(Pill pill) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        Intent intent = new Intent(this, PillReminderBroadcastReceiver.class);
        intent.putExtra(Pill.PILL_INSTANCE, pill);


        PendingIntent pendingIntent = PendingIntent.getService(this, (int) pill.id, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        alarmManager.cancel(pendingIntent);


    }

    private void startAlarm(Pill pill) {
        if (mCurrentPill != null) {
            stopCurrentAlarm();
        }
        if (pill.getVibrate()) {
            AlarmHelper.startSoundAndVibration(this, pill);
        }


        mCurrentPill = pill;


    }

    private void scheduleNextAlarm(Pill pill) {
        if (pill.getRepeat()) {
            pill.setScheduleTommorow(true);
            createAlarm(pill);
        } else {
            Pill.updatePill(Pill.ALARM_DISABLED, Pill$Table.ENABLED, pill.id);
            EventBus.getDefault().post(new PillEnabledEvent());
        }
    }

    private void stopCurrentAlarm() {
        if (mCurrentPill == null) {
            return;
        }
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.cancel((int) mCurrentPill.id);
        deleteAlarm(mCurrentPill);
        scheduleNextAlarm(mCurrentPill);
        AlarmHelper.stopSoundAndVibration(this);
        mCurrentPill = null;

    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


}
