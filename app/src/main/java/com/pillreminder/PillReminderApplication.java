package com.pillreminder;

import android.app.Application;

import com.raizlabs.android.dbflow.config.FlowManager;

public class PillReminderApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FlowManager.init(this);

    }
}
