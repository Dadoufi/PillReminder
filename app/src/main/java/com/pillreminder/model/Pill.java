package com.pillreminder.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.pillreminder.data.PillManager;
import com.pillreminder.ui.PillFragment;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.list.FlowQueryList;
import com.raizlabs.android.dbflow.runtime.DBTransactionInfo;
import com.raizlabs.android.dbflow.runtime.TransactionManager;
import com.raizlabs.android.dbflow.runtime.transaction.BaseTransaction;
import com.raizlabs.android.dbflow.runtime.transaction.UpdateTransaction;
import com.raizlabs.android.dbflow.sql.builder.Condition;
import com.raizlabs.android.dbflow.sql.builder.ConditionQueryBuilder;
import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


@Table(databaseName = PillManager.NAME)
public class Pill extends BaseModel implements Parcelable {

    public static final String PILL_INSTANCE = "PILL_INSTANCE";
    public static final String ALL_PILL_INSTANCES = "ALL_PILL_INSTANCES";


    public static final int REPEAT_ENABLED = 1;
    public static final int REPEAT_DISABLED = 0;
    public static final int VIBRATE_ENABLED = 1;
    public static final int VIBRATE_DISABLED = 0;
    public static final int ALARM_ENABLED = 1;
    public static final int ALARM_DISABLED = 0;
    public static final Creator<Pill> CREATOR = new Creator<Pill>() {
        public Pill createFromParcel(Parcel source) {
            return new Pill(source);
        }

        public Pill[] newArray(int size) {
            return new Pill[size];
        }
    };
    @Column
    @PrimaryKey(autoincrement = true)
    public long id;
    @Column
    public String name;
    @Column
    public int repeat;
    @Column
    public int hour;
    @Column
    public int minute;
    @Column
    public String ringtone;
    @Column
    public int vibrate;
    @Column
    public int enabled;

    private boolean isScheduleTommorow;


    private Pill(Builder builder) {
        setName(builder.name);
        setRepeat(builder.repeat);
        setHour(builder.hour);
        setMinute(builder.minute);
        setRingtone(builder.ringtone);
        vibrate = builder.vibrate;
        enabled = builder.enabled;
    }

    public Pill(long id, String name, int hour, int minute, String ringtone, int vibrate, int repeat) {
        this.id = id;
        this.name = name;
        this.hour = hour;
        this.minute = minute;
        this.ringtone = ringtone;
        this.vibrate = vibrate;
        this.repeat = repeat;
    }


    public Pill(String name, int hour, int minute, String ringtone, int vibrate, int repeat, int enabled) {
        this.name = name;
        this.hour = hour;
        this.minute = minute;
        this.ringtone = ringtone;
        this.vibrate = vibrate;
        this.repeat = repeat;
    }


    public Pill() {
    }


    protected Pill(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.repeat = in.readInt();
        this.hour = in.readInt();
        this.minute = in.readInt();
        this.ringtone = in.readString();
        this.vibrate = in.readInt();
        this.enabled = in.readInt();
    }

    public static List<Pill> getAllPills() {

        return new Select().all().from(Pill.class).queryList();
    }

    public static FlowQueryList<Pill> getAllPill() {

        return new Select().all().from(Pill.class).queryTableList();
    }

    public static Pill getPillById(long id) {

        Pill pill = new Select().from(Pill.class).where(
                Condition.column(Pill$Table.ID).is(id)).querySingle();


        return pill;
    }

    public static void deletePill(long id) {
        new Delete()
                .from(Pill.class)
                .where(Condition.column(Pill$Table.ID).is(id))
                .queryClose();
    }

    public static void updatePill(Object value, String columnAlias, long id) {


//        Where update = new Update<Pill>(Pill.class).set(Condition.column(columnAlias).eq(value))
//                .where(Condition.column(Pill$Table.ID).is(id));
//        update.queryClose();

        ConditionQueryBuilder set = new ConditionQueryBuilder<Pill>(Pill.class, Condition.column(columnAlias).eq(value));
        ConditionQueryBuilder where = new ConditionQueryBuilder<Pill>(Pill.class, Condition.column(Pill$Table.ID).is(id));


// TransactionManager (more methods similar to this one)
        TransactionManager.getInstance().addTransaction(new UpdateTransaction<Pill>(DBTransactionInfo.create(BaseTransaction.PRIORITY_UI), where, set));

    }


    public static Builder newBuilder() {
        return new Builder();
    }

    public Calendar getAlarmTime() {
        Date date = new Date();
        Calendar calendarAlarm = Calendar.getInstance();
        Calendar calendarNow = Calendar.getInstance();
        calendarNow.setTime(date);
        calendarAlarm.set(Calendar.HOUR_OF_DAY, hour);
        calendarAlarm.set(Calendar.MINUTE, minute);
        calendarAlarm.set(Calendar.SECOND, 0);
        calendarAlarm.set(Calendar.MILLISECOND, 0);

        if (calendarAlarm.before(calendarNow)) {
            calendarAlarm.add(Calendar.DATE, 1);
        }
        return calendarAlarm;
    }

    public Calendar getTomorrowAlarmTime() {
        Date date = new Date();

        Calendar calendarAlarm = Calendar.getInstance();
        Calendar calendarNow = Calendar.getInstance();
        calendarNow.setTime(date);
        calendarAlarm.add(Calendar.DATE, 1);
        calendarAlarm.set(Calendar.HOUR_OF_DAY, hour);
        calendarAlarm.set(Calendar.MINUTE, minute);
        calendarAlarm.set(Calendar.SECOND, 0);
        calendarAlarm.set(Calendar.MILLISECOND, 0);


        return calendarAlarm;
    }

    public boolean isScheduleTommorow() {
        return isScheduleTommorow;
    }

    public void setScheduleTommorow(boolean scheduleTommorow) {
        isScheduleTommorow = scheduleTommorow;
    }

    public void setRepeat(int repeat) {
        this.repeat = repeat;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public String getRingtone() {
        return ringtone;
    }

    public void setRingtone(String ringtone) {
        this.ringtone = ringtone;
    }

    public boolean getVibrate() {
        return vibrate == VIBRATE_ENABLED;
    }

    public void setVibrate(boolean vibrate) {
        if (vibrate) {
            this.vibrate = VIBRATE_ENABLED;
        } else {
            this.vibrate = VIBRATE_DISABLED;
        }

    }

    public Uri getRingtoneUri() {
        if (ringtone.equals(PillFragment.NO_STRING_URI)) {
            return PillFragment.NO_RINGTONE_URI;
        }
        return Uri.parse(ringtone);
    }

    public boolean getRepeat() {

        return repeat == REPEAT_ENABLED;
    }

    public void setRepeat(boolean repeat) {
        if (repeat) {
            this.repeat = REPEAT_ENABLED;
        } else {
            this.enabled = REPEAT_DISABLED;
        }


    }

    public boolean getEnabled() {
        return enabled == ALARM_ENABLED;
    }

    public void setEnabled(boolean enabled) {
        if (enabled) {
            this.enabled = ALARM_ENABLED;
        } else {
            this.enabled = ALARM_DISABLED;
        }

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeInt(this.repeat);
        dest.writeInt(this.hour);
        dest.writeInt(this.minute);
        dest.writeString(this.ringtone);
        dest.writeInt(this.vibrate);
        dest.writeInt(this.enabled);
    }

    public static final class Builder {
        private String name;
        private int repeat;
        private int hour;
        private int minute;
        private String ringtone;
        private int vibrate;
        private int enabled;

        private Builder() {
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder repeat(int val) {
            repeat = val;
            return this;
        }

        public Builder hour(int val) {
            hour = val;
            return this;
        }

        public Builder minute(int val) {
            minute = val;
            return this;
        }

        public Builder ringtone(String val) {
            ringtone = val;
            return this;
        }

        public Builder vibrate(int val) {
            vibrate = val;
            return this;
        }

        public Builder enabled(int val) {
            enabled = val;
            return this;
        }

        public Pill build() {
            return new Pill(this);
        }
    }
}
