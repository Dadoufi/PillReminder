package com.pillreminder.ui;


import android.app.Activity;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.pillreminder.CustomItemClickListener;
import com.pillreminder.PillAdapter;
import com.pillreminder.R;
import com.pillreminder.Utils;
import com.pillreminder.alarm.AlarmPillService;
import com.pillreminder.events.PillEnabledEvent;
import com.pillreminder.model.Pill;
import com.raizlabs.android.dbflow.list.FlowQueryList;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PillFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PillFragment extends Fragment implements CustomItemClickListener {

    public static final Uri NO_RINGTONE_URI = Uri.EMPTY;
    public static final String NO_STRING_URI = "NO_STRING_URI";
    private static final String SEL_AUDIO_SRC = "audio/*";
    private static final int SEL_SRC_RINGTONE = 0;
    private static final int REQUEST_CODE_RINGTONE = 1;
    @Bind(R.id.alarms_empty_view)
    TextView alarmsEmptyView;
    @Bind(R.id.pill_list)
    RecyclerView pillRecyclerView;
    private int mSelectSource = SEL_SRC_RINGTONE;
    private LinearLayoutManager mLayoutManager;
    private PillAdapter adapter;
    private FlowQueryList<Pill> pillList;
    private Pill mCurrentPill;
    private int mCurrentPosition;



    private Calendar calendar;


    public PillFragment() {
    }

    public static PillFragment newInstance() {
        return new PillFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        calendar = Calendar.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pill, container, false);
        ButterKnife.bind(this, view);


        pillList = Pill.getAllPill();


        if (pillList.isEmpty()) {
            //TransactionManager.getInstance().addTransaction(new SaveModelTransaction<>(ProcessModelInfo.withModels(pillList)));
        }


        mLayoutManager = new LinearLayoutManager(getActivity());
        adapter = new PillAdapter(getActivity(), pillList, pillRecyclerView, this);
        pillRecyclerView.setLayoutManager(mLayoutManager);
        pillRecyclerView.setAdapter(adapter);

        toggleEmptyVisibility();


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void toggleEmptyVisibility() {
        if (pillList.isEmpty()) {
            alarmsEmptyView.setVisibility(View.VISIBLE);
        } else {
            alarmsEmptyView.setVisibility(View.GONE);
        }
    }

    public void createNewReminder() {
        Pill pill = Pill.newBuilder()
                .name(getResources().getString(R.string.pill_hint))
                .hour(calendar.get(Calendar.HOUR))
                .minute(calendar.get(Calendar.MINUTE))
                .ringtone(com.pillreminder.Utils.getSystemDefaultAlarm(getActivity()).toString())
                .vibrate(Pill.VIBRATE_ENABLED)
                .repeat(Pill.REPEAT_DISABLED)
                .enabled(Pill.ALARM_DISABLED).build();
        pillList.add(pill);
        adapter.setAlarmExpanded(pill);
        adapter.notifyItemInserted(pillList.size() - 1);
        mLayoutManager.scrollToPosition(pillList.size() - 1);
        toggleEmptyVisibility();
        //pillList.refresh();


    }

    @Override
    public void onItemClick(View v, final int position, final PillAdapter.ViewHolder viewHolder, final Pill pill) {
        this.mCurrentPill = pill;
        this.mCurrentPosition = position;
        switch (v.getId()) {
            case R.id.digital_clock:
                TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
                                pill.hour = hourOfDay;
                                pill.minute = minute;
                                updateAlarm(pill, position);
                            }
                        },
                        pill.getHour(),
                        pill.getMinute(), viewHolder.digitalClock.is24hourFormat()
                );
                timePickerDialog.setThemeDark(true);
                timePickerDialog.show(getActivity().getFragmentManager(), "Datepickerdialog");

                break;
            case R.id.choose_ringtone:

                launchRingTonePicker(pill, position);
                break;
            case R.id.edit_label:
                showLabelDialog(pill, position);
                break;

            case R.id.label:
                showLabelDialog(pill, position);
                break;

            case R.id.delete:
                // Pill.deletePill(pill.getId());
                pillList.remove(pill);
                adapter.notifyItemRemoved(position);
                mLayoutManager.scrollToPosition(position - 1);
                toggleEmptyVisibility();
                pillList.refresh();
                AlarmPillService.deleteAlarm(getActivity(), pill);
                adapter.onItemDeleted();


                ((MainActivity) getActivity()).toggleFabVisibility(View.VISIBLE);


                break;

        }


    }


    @Override
    public void onItemChecked(CompoundButton compoundButton, boolean checked, int position, PillAdapter.ViewHolder viewHolder, Pill pill) {
        switch (compoundButton.getId()) {
            case R.id.onoff:
                if (checked && !pill.getEnabled()) {
                    AlarmPillService.createAlarm(getActivity(), pill);
                    Utils.popAlarmSetToast(getActivity(), pill.getAlarmTime().getTimeInMillis());
                } else if (pill.getEnabled() && !checked) {
                    AlarmPillService.deleteAlarm(getActivity(), pill);
                }
                break;

            case R.id.repeat_onoff:
                if (pill.getRepeat() != checked)
                    AlarmPillService.createAlarm(getActivity(), pill);
                break;
            case R.id.vibrate_onoff:
                if (pill.getVibrate() != checked)
                    AlarmPillService.createAlarm(getActivity(), pill);
                break;
        }

    }


    private void launchRingTonePicker(final Pill pill, final int position) {
        PillFragment.this.mSelectSource = SEL_SRC_RINGTONE;
        PillFragment.this.sendPickIntent(pill, position);
    }

    private void sendPickIntent(Pill pill, int position) {
        if (mSelectSource == SEL_SRC_RINGTONE) {
            Uri oldRingtone = NO_RINGTONE_URI.equals(
                    pill.getRingtoneUri()) ? null : pill.getRingtoneUri();
            final Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
            intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI,
                    oldRingtone);
            intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE,
                    RingtoneManager.TYPE_ALARM);
            intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT,
                    false);

            startActivityForResult(intent, REQUEST_CODE_RINGTONE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_RINGTONE) {

                Uri uri = intent.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
                String ringtone = uri == null ? NO_STRING_URI : uri.toString();

                if (!mCurrentPill.ringtone.equals(ringtone)) {
                    mCurrentPill.ringtone = ringtone;
                    updateAlarm(mCurrentPill, mCurrentPosition);
                }


            }
        }
    }


    private void showLabelDialog(final Pill pill, final int position) {
        MaterialDialog materialDialog = new MaterialDialog.Builder(getContext())
                .title(R.string.pill_name)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .autoDismiss(false)
                .theme(Theme.DARK)
                .input(null, null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog materialDialog, CharSequence charSequence) {
                        if (charSequence.length() < 1) {
                            materialDialog.getInputEditText().setError(getString(R.string.pill_name_length_error));
                        }
                    }
                })
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                        String name = materialDialog.getInputEditText().getText().length() > 0 ? materialDialog.getInputEditText().getText().toString() : "";
                        if (!name.equals(pill.name)) {
                            pill.name = name;
                            updateAlarm(pill, position);
                        }

                        materialDialog.dismiss();
                    }
                }).build();
        if (pill.getName() != null) {
            materialDialog.getInputEditText().setText(pill.getName());
        }

        materialDialog.show();


    }


    private void updateAlarm(Pill pill, int position) {
        pillList.set(pill);
        pillList.refresh();
        adapter.notifyItemChanged(position);
        if (pill.getEnabled()) {
            AlarmPillService.createAlarm(getActivity(), pill);
        }
    }


    public void onEvent(PillEnabledEvent event) {
        if (pillList != null && adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }
}
