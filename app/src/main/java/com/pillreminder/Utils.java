package com.pillreminder;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.format.DateFormat;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import android.widget.TextClock;
import android.widget.Toast;

import com.pillreminder.model.Pill;

import java.util.Calendar;
import java.util.Locale;


public class Utils {


    public static final String DOC_EXTERNAL = "com.android.externalstorage.documents";
    public static final Uri NO_RINGTONE_URI = Uri.EMPTY;
    public static final String LABEL = "label";
    public static final String FRAG_TAG_TIME_PICKER = "time_dialog";
    private static final float TINTED_LEVEL = 0.1f;
    public static TypedArray sBackgroundSpectrum;
    private static int sDefaultBackgroundSpectrumColor;

    public static void setTimeFormat(TextClock clock, int amPmFontSize) {
        if (clock != null) {
            // Get the best format for 12 hours mode according to the locale
            clock.setFormat12Hour(get12ModeFormat(amPmFontSize));
            // Get the best format for 24 hours mode according to the locale
            clock.setFormat24Hour(get24ModeFormat());
        }
    }

    /***
     * @param amPmFontSize - size of am/pm label (label removed is size is 0).
     * @return format string for 12 hours mode time
     */
    public static CharSequence get12ModeFormat(int amPmFontSize) {
        String skeleton = "hma";
        String pattern = DateFormat.getBestDateTimePattern(Locale.getDefault(), skeleton);
        // Remove the am/pm
        if (amPmFontSize <= 0) {
            pattern.replaceAll("a", "").trim();
        }
        // Replace spaces with "Hair Space"
        pattern = pattern.replaceAll(" ", "\u200A");
        // Build a spannable so that the am/pm will be formatted
        int amPmPos = pattern.indexOf('a');
        if (amPmPos == -1) {
            return pattern;
        }
        Spannable sp = new SpannableString(pattern);
        sp.setSpan(new StyleSpan(Typeface.NORMAL), amPmPos, amPmPos + 1,
                Spannable.SPAN_POINT_MARK);
        sp.setSpan(new AbsoluteSizeSpan(amPmFontSize), amPmPos, amPmPos + 1,
                Spannable.SPAN_POINT_MARK);
        sp.setSpan(new TypefaceSpan("sans-serif"), amPmPos, amPmPos + 1,
                Spannable.SPAN_POINT_MARK);
        return sp;
    }

    public static CharSequence get24ModeFormat() {
        String skeleton = "Hm";
        return DateFormat.getBestDateTimePattern(Locale.getDefault(), skeleton);
    }

    public static String getTitleColumnNameForUri(Uri uri) {

        if (DOC_EXTERNAL.equals(uri.getAuthority())) {
            return DocumentsContract.Document.COLUMN_DISPLAY_NAME;
        }
        return MediaStore.Audio.Media.TITLE;
    }

    public static Uri getSystemDefaultAlarm(Context context) {

        return RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
    }

    public static String getFormattedTime(Context context, Calendar time) {
        String skeleton = DateFormat.is24HourFormat(context) ? "EHm" : "Ehma";
        String pattern = DateFormat.getBestDateTimePattern(Locale.getDefault(), skeleton);
        return (String) DateFormat.format(pattern, time);
    }

    public static String getAlarmText(Context context, Pill pill) {
        String alarmTimeStr = getFormattedTime(context, pill.getAlarmTime());
        return !pill.getName().isEmpty() ? alarmTimeStr + " - " + pill.getName()
                : alarmTimeStr;
    }



    /**
     * format "Alarm set for 2 days 7 hours and 53 minutes from
     * now"
     */
    private static String formatToast(Context context, long timeInMillis) {
        long delta = timeInMillis - System.currentTimeMillis();
        long hours = delta / (1000 * 60 * 60);
        long minutes = delta / (1000 * 60) % 60;
        long days = hours / 24;
        hours = hours % 24;

        String daySeq = (days == 0) ? "" :
                (days == 1) ? context.getString(R.string.day) :
                        context.getString(R.string.days, Long.toString(days));

        String minSeq = (minutes == 0) ? "" :
                (minutes == 1) ? context.getString(R.string.minute) :
                        context.getString(R.string.minutes, Long.toString(minutes));

        String hourSeq = (hours == 0) ? "" :
                (hours == 1) ? context.getString(R.string.hour) :
                        context.getString(R.string.hours, Long.toString(hours));

        boolean dispDays = days > 0;
        boolean dispHour = hours > 0;
        boolean dispMinute = minutes > 0;

        int index = (dispDays ? 1 : 0) |
                (dispHour ? 2 : 0) |
                (dispMinute ? 4 : 0);

        String[] formats = context.getResources().getStringArray(R.array.reminder_set);
        return String.format(formats[index], daySeq, hourSeq, minSeq);
    }

    public static void popAlarmSetToast(Context context, long timeInMillis) {
        String toastText = formatToast(context, timeInMillis);
        Toast toast = Toast.makeText(context, toastText, Toast.LENGTH_LONG);
        ToastMaster.setToast(toast);
        toast.show();
    }

    public static String getRingToneTitle(Context context, Pill pill) {
        String title = "";
        Ringtone ringTone = RingtoneManager.getRingtone(context, pill.getRingtoneUri());
        if (ringTone != null) {
            title = ringTone.getTitle(context);
        }
        return title;
    }

    private static void loadBackgroundSpectrum(Context context) {
        Resources res = context.getResources();
        sBackgroundSpectrum = res.obtainTypedArray(R.array.background_color_by_hour);
        sDefaultBackgroundSpectrumColor = res.getColor(R.color.hour_12);
    }

    public static int getCurrentHourColor(Context context) {
        if (sBackgroundSpectrum == null) {
            loadBackgroundSpectrum(context);
        }
        final int hourOfDay = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        return sBackgroundSpectrum.getColor(hourOfDay, sDefaultBackgroundSpectrumColor);
    }

    public static int getTintedBackgroundColor(Context context) {
        final int c = Utils.getCurrentHourColor(context);
        final int red = Color.red(c) + (int) (TINTED_LEVEL * (255 - Color.red(c)));
        final int green = Color.green(c) + (int) (TINTED_LEVEL * (255 - Color.green(c)));
        final int blue = Color.blue(c) + (int) (TINTED_LEVEL * (255 - Color.blue(c)));
        return Color.rgb(red, green, blue);
    }


}
