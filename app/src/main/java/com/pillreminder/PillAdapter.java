package com.pillreminder;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import com.pillreminder.model.Pill;
import com.pillreminder.ui.PillFragment;
import com.pillreminder.views.EllipsizeLayout;
import com.pillreminder.views.TextTime;
import com.raizlabs.android.dbflow.list.FlowQueryList;

import java.util.HashSet;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PillAdapter extends RecyclerView.Adapter<PillAdapter.ViewHolder> {


    private static final float EXPAND_DECELERATION = 1f;
    private static final float COLLAPSE_DECELERATION = 0.7f;
    private static final long INVALID_ID = -1;
    private static final int ROTATE_180_DEGREE = 180;
    private static final float ALARM_ELEVATION = 8f;
    private static final int ANIMATION_DURATION = 300;
    private static final int EXPAND_DURATION = 300;
    private static final int COLLAPSE_DURATION = 250;


    private final RecyclerView mList;
    private final HashSet<Long> mRepeatChecked = new HashSet<Long>();
    private final HashSet<Long> mSelectedAlarms = new HashSet<Long>();
    private final boolean mHasVibrator;
    private final int mCollapseExpandHeight;
    private ViewHolder mExpandedItemHolder;
    private ListView mAlarmsList;
    private long mExpandedId;
    private Interpolator mExpandInterpolator;
    private Interpolator mCollapseInterpolator;

    private Context mContext;
    private FlowQueryList<Pill> mPillList;
    private Pill mPill;
    private CustomItemClickListener mListener;


    private long mScrollAlarmId = INVALID_ID;
    private final Runnable mScrollRunnable = new Runnable() {
        @Override
        public void run() {
            if (mScrollAlarmId != INVALID_ID) {
                View v = getViewById(mScrollAlarmId, mPill);
                if (v != null) {
                    Rect rect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                    mList.requestChildRectangleOnScreen(v, rect, false);
                }
                mScrollAlarmId = INVALID_ID;
            }
        }
    };

    public PillAdapter(Context context, FlowQueryList<Pill> pillList, RecyclerView mList, CustomItemClickListener mListener) {

        this.mContext = context;
        this.mPillList = pillList;
        this.mList = mList;
        this.mListener = mListener;
        //this.mPillList.enableSelfRefreshes(this.mContext);
        mExpandInterpolator = new DecelerateInterpolator(EXPAND_DECELERATION);
        mCollapseInterpolator = new DecelerateInterpolator(COLLAPSE_DECELERATION);

        mHasVibrator = ((Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE))
                .hasVibrator();

        mCollapseExpandHeight = (int) mContext.getResources().getDimension(R.dimen.collapse_expand_height);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pill, parent, false);

        final ViewHolder viewHolder = new ViewHolder(view);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Pill pill = mPillList.get(viewHolder.getAdapterPosition());
                mListener.onItemClick(v, viewHolder.getAdapterPosition(), viewHolder, pill);
                if (isAlarmExpanded(pill)) {
                    collapseAlarm(viewHolder, true);
                } else {
                    expandAlarm(viewHolder, true, pill);
                }
            }
        });

        viewHolder.label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Pill pill = mPillList.get(viewHolder.getAdapterPosition());
                mListener.onItemClick(v, viewHolder.getAdapterPosition(), viewHolder, pill);
            }
        });


        viewHolder.digitalClock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Pill pill = mPillList.get(viewHolder.getAdapterPosition());
                mListener.onItemClick(v, viewHolder.getAdapterPosition(), viewHolder, pill);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        expandAlarm(viewHolder, true, pill);
                    }
                }, 100);

                viewHolder.alarmItem.post(mScrollRunnable);


            }
        });
        viewHolder.editLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Pill pill = mPillList.get(viewHolder.getAdapterPosition());
                mListener.onItemClick(v, viewHolder.getAdapterPosition(), viewHolder, pill);


            }
        });


        viewHolder.onoff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                final Pill pill = mPillList.get(viewHolder.getAdapterPosition());
                mListener.onItemChecked(buttonView, isChecked, viewHolder.getAdapterPosition(), viewHolder, pill);
                if (isChecked != pill.getEnabled()) {
                    setDigitalTimeAlpha(viewHolder, isChecked);
                }
                pill.setEnabled(isChecked);
                mPillList.set(pill);
            }
        });

        viewHolder.repeatOnoff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                final Pill pill = mPillList.get(viewHolder.getAdapterPosition());
                mListener.onItemChecked(buttonView, isChecked, viewHolder.getAdapterPosition(), viewHolder, pill);
                pill.setRepeat(isChecked);
                mPillList.set(pill);
            }
        });

        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Pill pill = mPillList.get(viewHolder.getAdapterPosition());
                mListener.onItemClick(v, viewHolder.getAdapterPosition(), viewHolder, pill);

            }
        });

        viewHolder.chooseRingtone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Pill pill = mPillList.get(viewHolder.getAdapterPosition());
                mListener.onItemClick(v, viewHolder.getAdapterPosition(), viewHolder, pill);

            }
        });

        viewHolder.vibrateOnoff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                final Pill pill = mPillList.get(viewHolder.getAdapterPosition());
                mListener.onItemChecked(buttonView, isChecked, viewHolder.getAdapterPosition(), viewHolder, pill);
                pill.setVibrate(isChecked);
                mPillList.set(pill);
            }
        });


        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {
        final Pill pill = getPill(position);
        mPill = pill;


        viewHolder.onoff.setChecked(pill.getEnabled());

        if (pill.getEnabled()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //setAlarmItemBackgroundAndElevation(viewHolder.alarmItem, true /* expanded */);
            }
            setDigitalTimeAlpha(viewHolder, true);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //setAlarmItemBackgroundAndElevation(viewHolder.alarmItem, false /* expanded */);
            }
            setDigitalTimeAlpha(viewHolder, viewHolder.onoff.isChecked());
        }
        viewHolder.digitalClock.setFormat(
                (int) mContext.getResources().getDimension(R.dimen.alarm_label_size));
        viewHolder.digitalClock.setTime(pill.getHour(), pill.getMinute());
        viewHolder.digitalClock.setClickable(true);


        boolean expanded = isAlarmExpanded(pill);
        if (expanded) {
            mExpandedItemHolder = viewHolder;
        }
        viewHolder.expandArea.setVisibility(expanded ? View.VISIBLE : View.GONE);
        viewHolder.delete.setVisibility(expanded ? View.VISIBLE : View.GONE);
        viewHolder.summary.setVisibility(expanded ? View.GONE : View.VISIBLE);
        viewHolder.hairline.setVisibility(expanded ? View.GONE : View.VISIBLE);
        viewHolder.arrow.setRotation(expanded ? ROTATE_180_DEGREE : 0);

        // Set the repeat text or leave it blank if it does not repeat.


        if (pill.getName() != null && pill.getName().length() != 0) {
            viewHolder.label.setText(pill.getName());
        } else {
            viewHolder.label.setText(R.string.pill_hint);
        }
        viewHolder.label.setVisibility(View.VISIBLE);


        if (expanded) {
            expandAlarm(viewHolder, false, pill);
        }


    }


    public void postRunnableScrollable(ViewHolder viewHolder) {
        viewHolder.alarmItem.post(mScrollRunnable);
    }

    public void expandAlarm(final ViewHolder viewHolder, boolean animate, Pill pill) {
        // Skip animation later if item is already expanded
        animate &= mExpandedId != pill.id;

        if (mExpandedItemHolder != null
                && mExpandedItemHolder != viewHolder
                && mExpandedId != pill.id) {
            // Only allow one alarm to expand at a time.
            collapseAlarm(mExpandedItemHolder, animate);
        }

        bindExpandArea(viewHolder, pill);

        mExpandedId = pill.id;
        mExpandedItemHolder = viewHolder;

        // Scroll the view to make sure it is fully viewed
        mScrollAlarmId = pill.id;

        // Save the starting height so we can animate from this value.
        final int startingHeight = viewHolder.alarmItem.getHeight();

        // Set the expand area to visible so we can measure the height to animate to.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setAlarmItemBackgroundAndElevation(viewHolder.alarmItem, true /* expanded */);
        }
        viewHolder.expandArea.setVisibility(View.VISIBLE);
        viewHolder.delete.setVisibility(View.VISIBLE);

        if (!animate) {
            // Set the "end" layout and don't do the animation.
            viewHolder.arrow.setRotation(ROTATE_180_DEGREE);
            return;
        }

        // Add an onPreDrawListener, which gets called after measurement but before the draw.
        // This way we can check the height we need to animate to before any drawing.
        // Note the series of events:
        //  * expandArea is set to VISIBLE, which causes a layout pass
        //  * the view is measured, and our onPreDrawListener is called
        //  * we set up the animation using the start and end values.
        //  * the height is set back to the starting point so it can be animated down.
        //  * request another layout pass.
        //  * return false so that onDraw() is not called for the single frame before
        //    the animations have started.
        final ViewTreeObserver observer = mList.getViewTreeObserver();
        observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                // We don't want to continue getting called for every listview drawing.
                if (observer.isAlive()) {
                    observer.removeOnPreDrawListener(this);
                }
                // Calculate some values to help with the animation.
                final int endingHeight = viewHolder.alarmItem.getHeight();
                final int distance = endingHeight - startingHeight;
                final int collapseHeight = viewHolder.collapseExpand.getHeight();

                // Set the height back to the start state of the animation.
                viewHolder.alarmItem.getLayoutParams().height = startingHeight;
                // To allow the expandArea to glide in with the expansion animation, set a
                // negative top margin, which will animate down to a margin of 0 as the height
                // is increased.
                // Note that we need to maintain the bottom margin as a fixed value (instead of
                // just using a listview, to allow for a flatter hierarchy) to fit the bottom
                // bar underneath.
                FrameLayout.LayoutParams expandParams = (FrameLayout.LayoutParams)
                        viewHolder.expandArea.getLayoutParams();
                expandParams.setMargins(0, -distance, 0, collapseHeight);
                viewHolder.alarmItem.requestLayout();

                // Set up the animator to animate the expansion.
                ValueAnimator animator = ValueAnimator.ofFloat(0f, 1f)
                        .setDuration(EXPAND_DURATION);
                animator.setInterpolator(mExpandInterpolator);
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animator) {
                        Float value = (Float) animator.getAnimatedValue();

                        // For each value from 0 to 1, animate the various parts of the layout.
                        viewHolder.alarmItem.getLayoutParams().height =
                                (int) (value * distance + startingHeight);
                        FrameLayout.LayoutParams expandParams = (FrameLayout.LayoutParams)
                                viewHolder.expandArea.getLayoutParams();
                        expandParams.setMargins(
                                0, (int) -((1 - value) * distance), 0, collapseHeight);
                        viewHolder.arrow.setRotation(ROTATE_180_DEGREE * value);
                        viewHolder.summary.setAlpha(1 - value);
                        viewHolder.hairline.setAlpha(1 - value);

                        viewHolder.alarmItem.requestLayout();
                    }
                });
                // Set everything to their final values when the animation's done.
                animator.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        // Set it back to wrap content since we'd explicitly set the height.
                        viewHolder.alarmItem.getLayoutParams().height =
                                ViewGroup.LayoutParams.WRAP_CONTENT;
                        viewHolder.arrow.setRotation(ROTATE_180_DEGREE);
                        viewHolder.summary.setVisibility(View.GONE);
                        viewHolder.hairline.setVisibility(View.GONE);
                        viewHolder.delete.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        // TODO we may have to deal with cancelations of the animation.
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                    }

                    @Override
                    public void onAnimationStart(Animator animation) {
                    }
                });
                animator.start();

                // Return false so this draw does not occur to prevent the final frame from
                // being drawn for the single frame before the animations start.
                return false;
            }
        });
    }

    public boolean isAlarmExpanded(Pill pill) {
        return mExpandedId == pill.id;
    }

    public void setAlarmExpanded(Pill pill) {
        this.mExpandedId = pill.id;
    }

    public void onItemDeleted() {
        mExpandedId = INVALID_ID;
        mExpandedItemHolder = null;
    }

    public void collapseAlarm(final ViewHolder viewHolder, boolean animate) {
        mExpandedId = INVALID_ID;
        mExpandedItemHolder = null;

        // Save the starting height so we can animate from this value.
        final int startingHeight = viewHolder.alarmItem.getHeight();

        // Set the expand area to gone so we can measure the height to animate to.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setAlarmItemBackgroundAndElevation(viewHolder.alarmItem, false /* expanded */);
        }
        viewHolder.expandArea.setVisibility(View.GONE);

        if (!animate) {
            // Set the "end" layout and don't do the animation.
            viewHolder.arrow.setRotation(0);
            viewHolder.hairline.setTranslationY(0);
            return;
        }

        // Add an onPreDrawListener, which gets called after measurement but before the draw.
        // This way we can check the height we need to animate to before any drawing.
        // Note the series of events:
        //  * expandArea is set to GONE, which causes a layout pass
        //  * the view is measured, and our onPreDrawListener is called
        //  * we set up the animation using the start and end values.
        //  * expandArea is set to VISIBLE again so it can be shown animating.
        //  * request another layout pass.
        //  * return false so that onDraw() is not called for the single frame before
        //    the animations have started.
        final ViewTreeObserver observer = mList.getViewTreeObserver();
        observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                if (observer.isAlive()) {
                    observer.removeOnPreDrawListener(this);
                }

                // Calculate some values to help with the animation.
                final int endingHeight = viewHolder.alarmItem.getHeight();
                final int distance = endingHeight - startingHeight;

                // Re-set the visibilities for the start state of the animation.
                viewHolder.expandArea.setVisibility(View.VISIBLE);
                viewHolder.delete.setVisibility(View.GONE);
                viewHolder.summary.setVisibility(View.VISIBLE);
                viewHolder.hairline.setVisibility(View.VISIBLE);
                viewHolder.summary.setAlpha(1);

                // Set up the animator to animate the expansion.
                ValueAnimator animator = ValueAnimator.ofFloat(0f, 1f)
                        .setDuration(COLLAPSE_DURATION);
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animator) {
                        Float value = (Float) animator.getAnimatedValue();

                        // For each value from 0 to 1, animate the various parts of the layout.
                        viewHolder.alarmItem.getLayoutParams().height =
                                (int) (value * distance + startingHeight);
                        FrameLayout.LayoutParams expandParams = (FrameLayout.LayoutParams)
                                viewHolder.expandArea.getLayoutParams();
                        expandParams.setMargins(
                                0, (int) (value * distance), 0, mCollapseExpandHeight);
                        viewHolder.arrow.setRotation(ROTATE_180_DEGREE * (1 - value));
                        viewHolder.delete.setAlpha(value);
                        viewHolder.summary.setAlpha(value);
                        viewHolder.hairline.setAlpha(value);

                        viewHolder.alarmItem.requestLayout();
                    }
                });
                animator.setInterpolator(mCollapseInterpolator);
                // Set everything to their final values when the animation's done.
                animator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        // Set it back to wrap content since we'd explicitly set the height.
                        viewHolder.alarmItem.getLayoutParams().height =
                                ViewGroup.LayoutParams.WRAP_CONTENT;

                        FrameLayout.LayoutParams expandParams = (FrameLayout.LayoutParams)
                                viewHolder.expandArea.getLayoutParams();
                        expandParams.setMargins(0, 0, 0, mCollapseExpandHeight);

                        viewHolder.expandArea.setVisibility(View.GONE);
                        viewHolder.arrow.setRotation(0);
                    }
                });
                animator.start();

                return false;
            }
        });
    }


    public int getViewTypeCount() {
        return 1;
    }

    private View getViewById(long id, Pill pill) {
        for (int i = 0; i < mList.getAdapter().getItemCount(); i++) {
            View v = mList.getChildAt(i);
            if (v != null && pill.id == id) {
                return v;
            }

        }
        return null;
    }


    private void bindExpandArea(final ViewHolder viewHolder, final Pill pill) {
        // Views in here are not bound until the item is expanded.

        if (pill.getName() != null && pill.getName().length() > 0) {
            viewHolder.editLabel.setText(pill.getName());
        } else {
            viewHolder.editLabel.setText(R.string.pill_name);
        }


        viewHolder.vibrateOnoff.setChecked(pill.getVibrate());
        viewHolder.repeatOnoff.setChecked(pill.getRepeat());


        final String ringtone;
        final String ringtitle;
        if (PillFragment.NO_STRING_URI.equals(pill.ringtone)) {
            ringtone = mContext.getResources().getString(R.string.silent_alarm_summary);
        } else {
            ringtitle = Utils.getRingToneTitle(mContext, pill);
            if (ringtitle != null) {
                ringtone = ringtitle;
            } else {
                ringtone = mContext.getResources().getString(R.string.silent_alarm_summary);
            }
        }

        viewHolder.chooseRingtone.setText(ringtone);
        viewHolder.chooseRingtone.setContentDescription(
                mContext.getResources().getString(R.string.ringtone_description) + " "
                        + pill.ringtone);

    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setAlarmItemBackgroundAndElevation(LinearLayout layout, boolean expanded) {
        if (expanded) {
            layout.setBackgroundColor(Utils.getTintedBackgroundColor(mContext));
            layout.setElevation(ALARM_ELEVATION);
        } else {
            layout.setBackgroundResource(R.drawable.alarm_background_normal);
            layout.setElevation(0);
        }
    }

    private void setDigitalTimeAlpha(ViewHolder holder, boolean enabled) {
        float alpha = enabled ? 1f : 0.69f;
        holder.digitalClock.setAlpha(alpha);
    }


    @Override
    public int getItemCount() {
        return mPillList == null ? 0 : mPillList.size();
    }

    public Pill getPill(int position) {
        return mPillList.get(position);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.digital_clock)
        public TextTime digitalClock;
        @Bind(R.id.onoff)
        Switch onoff;
        @Bind(R.id.repeat_onoff)
        CheckBox repeatOnoff;
        @Bind(R.id.choose_ringtone)
        TextView chooseRingtone;
        @Bind(R.id.vibrate_onoff)
        CheckBox vibrateOnoff;
        @Bind(R.id.edit_label)
        TextView editLabel;
        @Bind(R.id.expand_area)
        LinearLayout expandArea;
        @Bind(R.id.delete)
        ImageButton delete;
        @Bind(R.id.hairline)
        View hairline;
        @Bind(R.id.label)
        TextView label;
        @Bind(R.id.summary)
        EllipsizeLayout summary;
        @Bind(R.id.arrow)
        ImageView arrow;
        @Bind(R.id.collapse_expand)
        FrameLayout collapseExpand;
        @Bind(R.id.alarm_item)
        LinearLayout alarmItem;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);


        }


    }

}