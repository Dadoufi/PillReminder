package com.pillreminder;


import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Vibrator;

import com.pillreminder.model.Pill;
import com.pillreminder.ui.PillFragment;

public class AlarmHelper {


    private static final long[] sVibratePattern = new long[]{500, 500};
    private static boolean sStarted = false;
    private static MediaPlayer sPlayer;

    public static Intent createIntent(Context context, Class<?> cls, long id) {

        return new Intent(context, cls).putExtra(Pill.PILL_INSTANCE, id);

    }

    public static Intent createIntent(Context context, Class<?> cls) {

        return new Intent(context, cls);

    }

    public static void startSoundAndVibration(final Context context, Pill pill) {
        stopSoundAndVibration(context);

        if (pill.getVibrate()) {
            Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(sVibratePattern, 0);
            sStarted = true;
        }
        if (!pill.getRingtoneUri().equals(PillFragment.NO_RINGTONE_URI)) {
            sStarted = true;
            try {
                sPlayer = MediaPlayer.create(context, pill.getRingtoneUri());
                sPlayer.setLooping(true);
                //sPlayer.prepare();
                sPlayer.start();


            } catch (Exception e) {
                e.printStackTrace();
                sStarted = false;
            }
        }


    }


    public static void stopSoundAndVibration(Context context) {
        if (sStarted) {
            sStarted = false;

            if (sPlayer != null) {
                sPlayer.stop();
                sPlayer.release();
                sPlayer = null;

            }


            ((Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE)).cancel();
        }


    }


}
