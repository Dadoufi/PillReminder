package com.pillreminder;

import android.view.View;
import android.widget.CompoundButton;

import com.pillreminder.model.Pill;


public interface CustomItemClickListener {
    public void onItemClick(View v, int position, PillAdapter.ViewHolder viewHolder,Pill pill);

    public void onItemChecked(CompoundButton compoundButton,boolean isChecked, int position, PillAdapter.ViewHolder viewHolder, Pill pill);
}